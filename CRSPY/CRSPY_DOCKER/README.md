# CRSPY_DOCKER Example Files
A dockerized dev container for crspy has been put together showing how to use the code to automatically process different CRNS site data.

## Docker Scripts
Both bash and bat scripts are provided for OSX/Linux and Windows respectively.
These scripts grab the latest CRSPY docker image from dockerhub and try to run it in a virtual environment.
Warning since the image itself contains a condensed OS, it can be quite big (~1GB) when unpacked.
Thankfully docker handles all this for you so you don't need to keep track of where the downloaded URANOS images are going!

The simple tutorial script `run_linux.sh` just run the following command.
~~~
docker run -it  -p 8888:8888 johnpatrickstowell/crspy_tutorial
~~~
This command is doing the following:
- `docker run` Telling the docker service we want to start a job and to clone the URANOS image into a container to start it.
- `-it` Running interactively, so we can kill the job using `ctrl+C`.
- `-p 8888:8888` Exposure the Jupyter notebook port. You need access to this port on your network to run it.
- `johnpatrickstowell/crspy_tutorial` The actual image on docker hub we want to use. If there is a local image docker will try to copy that first before downloading it again from docker-hub.


To check it is working, try running one of these scripts on the command line first.

On Linux/OSX
~~~
source run_linux.sh
~~~

On Windows (note you can also double click the script)
~~~
run_windows.bat
~~~

This will produce the following output.
~~~
stowell$ ./run_linux.sh
[I 10:38:31.265 NotebookApp] Writing notebook server cookie secret to /root/.local/share/jupyter/runtime/notebook_cookie_secret
[W 2022-07-18 10:38:31.804 LabApp] 'port' has moved from NotebookApp to ServerApp. This config will be passed to ServerApp. Be sure to update your config before our next release.
[W 2022-07-18 10:38:31.804 LabApp] 'ip' has moved from NotebookApp to ServerApp. This config will be passed to ServerApp. Be sure to update your config before our next release.
[W 2022-07-18 10:38:31.804 LabApp] 'allow_root' has moved from NotebookApp to ServerApp. This config will be passed to ServerApp. Be sure to update your config before our next release.
[W 2022-07-18 10:38:31.804 LabApp] 'allow_root' has moved from NotebookApp to ServerApp. This config will be passed to ServerApp. Be sure to update your config before our next release.
[W 2022-07-18 10:38:31.804 LabApp] 'allow_root' has moved from NotebookApp to ServerApp. This config will be passed to ServerApp. Be sure to update your config before our next release.
[I 2022-07-18 10:38:31.814 LabApp] JupyterLab extension loaded from /usr/local/lib/python3.8/site-packages/jupyterlab
[I 2022-07-18 10:38:31.815 LabApp] JupyterLab application directory is /usr/local/share/jupyter/lab
[I 10:38:31.820 NotebookApp] Serving notebooks from local directory: /crspy
[I 10:38:31.820 NotebookApp] Jupyter Notebook 6.4.12 is running at:
[I 10:38:31.820 NotebookApp] http://deb444190c9a:8888/?token=a5a55bfb4cf291f84d1ab4ee2a6349dc4d6569fddb0c7f44
[I 10:38:31.820 NotebookApp]  or http://127.0.0.1:8888/?token=a5a55bfb4cf291f84d1ab4ee2a6349dc4d6569fddb0c7f44
[I 10:38:31.820 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 10:38:31.824 NotebookApp]

    To access the notebook, open this file in a browser:
        file:///root/.local/share/jupyter/runtime/nbserver-1-open.html
    Or copy and paste one of these URLs:
        http://deb444190c9a:8888/?token=a5a55bfb4cf291f84d1ab4ee2a6349dc4d6569fddb0c7f44
     or http://127.0.0.1:8888/?token=a5a55bfb4cf291f84d1ab4ee2a6349dc4d6569fddb0c7f44
~~~

As you can see it produces an access token `http://127.0.0.1:8888/?token=a5a55bfb4cf291f84d1ab4ee2a6349dc4d6569fddb0c7f44` that we can use in a browser to open the notebook. On your machine this may be slightly different depending on your IP. If you copy it into a browser however it should bring up the Jupyter notebook.

## Docker installation
Docker can be installed using the following link : https://docs.docker.com/get-docker/
This on windows may need to enable the WSL subsystem if it is not already enabled, and possibly hyper-virtualization.

Once docker is installed, start it by clicking `Docker Desktop` in the start menu on windows, or application menu on OSX.
On Linux docker should be setup as a service that starts on boot.

Finally you can check docker is running, by entering the following comment on a terminal/command-prompt.
~~~
$ docker run
latest: Pulling from library/hello-world
2db29710123e: Pull complete
Digest: sha256:53f1bbee2f52c39e41682ee1d388285290c5c8a76cc92b42687eecf38e0af3f0
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
~~~
