# CRSPY tutorial

This folder contains a python notebook and example data showing how to use CRSPY
to generate processed CRNS outputs for multiple sites at once.

## Installation
Running CRSPY locally requires the following python packages
~~~
pip install setuptools wheel Cython h5py jupyter notebook jupyterlab numpy==1.23
~~~

To install crspy a local copy of the repo has also been provided. Just run
~~~
cd crspy_package
python setup.py install
cd ../
~~~

## Running the tutorial
To run the workbook just run the following command to bring up Jupyter
~~~
jupyter nbclassic
~~~
