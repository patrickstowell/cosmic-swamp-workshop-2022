## COSMIC-SWAMP Software Workshop

This repo contains example files for the COSMIC-SWAMP workshop 07/2022.

## Contents
The tutorials contain the following
#### CRSPY
- `CRSPY_DOCKER` : Examples on how to run CRSPY using a Jupyter notebook in a self-contained docker image.
- `CRSPY_LOCAL` : Examples on how to run CRSPY using a local Jupyter notebook instance.
#### URANOS
- `URANOS_ANALYSIS` : Examples on how to process URANOS outputs using a Jupyter notebook. A self-contained docker image is also setup to run the example.
- `URANOS_DOCKER` : Examples on how to run URANOS using the dockerhub image.
- `URANOS_WINDOWS10_BINARY` : Collected URANOS requirements in a single zip file for Windows 10. For those who want a faster local installation.
