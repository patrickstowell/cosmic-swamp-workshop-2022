#!/bin/bash
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
xhost + $ip
docker run -it -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$ip:0 -v $PWD:/uranos/workdir/ -w /uranos/workdir/ johnpatrickstowell/uranos-ubuntu bash /runuranos
