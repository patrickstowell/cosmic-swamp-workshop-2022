# URANOS Data Analysis tutorial

Example Jupyter notebook showing how to use URANOS python processing library to handle the outputs of URANOS simulations.

## URANOS Analysis Docker installation

The notebook has already been setup in a self contained docker image.

If you just want to run the examples and understand the typical URANOS outputs:

On Linux
~~~
source run_linux.sh
~~~

On WINDOWS
~~~
./run_windows.bat
~~~

If you want to actually mount your own URANOS datasets into the docker image you can
mount your current working directory using the linux script.
~~~
run_linux_and_mount.sh
~~~

On windows this is also possible using the `-v` docker options, but it is beyond the scope of this tutorial.

If we run one of these scripts we should see the following:
~~~
stowell$ ./run_linux.sh
[I 10:40:43.041 NotebookApp] Writing notebook server cookie secret to /root/.local/share/jupyter/runtime/notebook_cookie_secret
[W 2022-07-18 10:40:43.619 LabApp] 'port' has moved from NotebookApp to ServerApp. This config will be passed to ServerApp. Be sure to update your config before our next release.
[W 2022-07-18 10:40:43.620 LabApp] 'ip' has moved from NotebookApp to ServerApp. This config will be passed to ServerApp. Be sure to update your config before our next release.
[W 2022-07-18 10:40:43.620 LabApp] 'allow_root' has moved from NotebookApp to ServerApp. This config will be passed to ServerApp. Be sure to update your config before our next release.
[W 2022-07-18 10:40:43.620 LabApp] 'allow_root' has moved from NotebookApp to ServerApp. This config will be passed to ServerApp. Be sure to update your config before our next release.
[W 2022-07-18 10:40:43.620 LabApp] 'allow_root' has moved from NotebookApp to ServerApp. This config will be passed to ServerApp. Be sure to update your config before our next release.
[I 2022-07-18 10:40:43.630 LabApp] JupyterLab extension loaded from /usr/local/lib/python3.8/site-packages/jupyterlab
[I 2022-07-18 10:40:43.631 LabApp] JupyterLab application directory is /usr/local/share/jupyter/lab
[I 10:40:43.637 NotebookApp] Serving notebooks from local directory: /uranos_analysis
[I 10:40:43.637 NotebookApp] Jupyter Notebook 6.4.12 is running at:
[I 10:40:43.637 NotebookApp] http://2521bd4e1c70:8888/?token=41d9c0a45d982f6e0376fc3bf9a2cf505b3a3d9b6c8020c1
[I 10:40:43.637 NotebookApp]  or http://127.0.0.1:8888/?token=41d9c0a45d982f6e0376fc3bf9a2cf505b3a3d9b6c8020c1
[I 10:40:43.638 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 10:40:43.642 NotebookApp]

    To access the notebook, open this file in a browser:
        file:///root/.local/share/jupyter/runtime/nbserver-1-open.html
    Or copy and paste one of these URLs:
        http://2521bd4e1c70:8888/?token=41d9c0a45d982f6e0376fc3bf9a2cf505b3a3d9b6c8020c1
     or http://127.0.0.1:8888/?token=41d9c0a45d982f6e0376fc3bf9a2cf505b3a3d9b6c8020c1
[I 10:40:49.712 NotebookApp] 302 GET /?token=41d9c0a45d982f6e0376fc3bf9a2cf505b3a3d9b6c8020c1 (172.17.0.1) 0.600000ms
~~~

As you can see it produces an access token `http://127.0.0.1:8888/?token=41d9c0a45d982f6e0376fc3bf9a2cf505b3a3d9b6c8020c1` that we can use in a browser to open the notebook. On your machine this may be slightly different depending on your IP. If you copy it into a browser however it should bring up the Jupyter notebook.

## Local runs

If you already have python and Jupyter installed then you may be able to just run the notebook directly.
The requirements are as follows
~~~
pip install setuptools wheel  Cython h5py jupyter notebook jupyterlab numpy==1.23 geopandas pillow scipy matplotlib
~~~

The command to run the notebook locally is
~~~
jupyter nbclassic
~~~




## Docker installation
Docker can be installed using the following link : https://docs.docker.com/get-docker/
This on windows may need to enable the WSL subsystem if it is not already enabled, and possibly hyper-virtualization.

Once docker is installed, start it by clicking `Docker Desktop` in the start menu on windows, or application menu on OSX.
On Linux docker should be setup as a service that starts on boot.

Finally you can check docker is running, by entering the following comment on a terminal/command-prompt.
~~~
$ docker run
latest: Pulling from library/hello-world
2db29710123e: Pull complete
Digest: sha256:53f1bbee2f52c39e41682ee1d388285290c5c8a76cc92b42687eecf38e0af3f0
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
~~~
