for /f "delims=[] tokens=2" %%a in ('ping -4 -n 1 %ComputerName% ^| findstr [') do set NetworkIP=%%a
echo Network IP: %NetworkIP%
docker run -it --rm -e DISPLAY=%NetworkIP%:0 -v %cd%:/uranos/workdir/ -w /uranos/workdir/ johnpatrickstowell/uranos-ubuntu bash /runuranos
cmd \k
