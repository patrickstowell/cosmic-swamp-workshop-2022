# URANOS Docker Files
URANOS Docker files showing how to run docker for various geometry configurations using the images straight from docker hub.

## Examples
Each of the Example folders contain an example configuration and the scripts required to run them using URANOS-docker.
Each one can be ran either with or without the GUI by starting the relevant script in each example directory for your specific OS.

- `Example_Edge` : Simple case with one large area of extremely wet ground (70% SM), and one dry area.
- `Example_Strip` : Simple case with a strip of dry ground, surrounded by wet soil.
- `Example_SM5` : Simple case with homogenous soil, 5%SM.
- `Example_SM25` : Simple case with homogenous soil, 25%SM.
- `Example_SM45` : Simple case with homogenous soil, 45%SM.
- `Example_TestFields` : More complex test case of different soil types based on M. Schroen's example on the URANOS github.

## Docker Scripts
Both bash and bat scripts are provided for OSX/Linux and Windows respectively.
These scripts grab the latest URANOS image from dockerhub and try to run it in a virtual environment.
Warning since the image itself contains a condensed OS, it can be quite big (~1GB) when unpacked.
Thankfully docker handles all this for you so you don't need to keep track of where the downloaded URANOS images are going!

The noGUI docker scripts show the commands needed to run URANOS using the config files found in the current directory.
~~~
docker run -it --rm -v $PWD:/uranos/workdir/ -w /uranos/workdir/ johnpatrickstowell/uranos-ubuntu bash /runuranos noGUI Uranos.cfg -platform offscreen
~~~
This command is doing the following:
- `docker run` Telling the docker service we want to start a job and to clone the URANOS image into a container to start it.
- `-it` Running interactively, so we can kill the job using `ctrl+C`.
- `--rm` Telling docker to delete the clone container once finished so we can save space.
- `-v $PWD:/uranos/workdir/` Telling docker to mount our current working directory into an absolute path into the container that URANOS is already setup to look for.
- `-w /uranos/workdir/` Telling docker to change our working directory to the absolute path we just mounted.
- `johnpatrickstowell/uranos-ubuntu` The actual image on docker hub we want to use. If there is a local image docker will try to copy that first before downloading it again from docker-hub.
- `/runuranos` A helper script inside the image itself to start URANOS with ROOT setup.
- `noGUI` Tell URANOS we don't want the GUI loaded.
- `platform offscreen` Tell Qt not to worry about setting up the GUI to save time.

To check URANOS-docker is working, try running one of the noGUI scripts on the command line first.

On Linux/OSX
~~~
source run_uranos_nogui_linux.sh
~~~

On Windows (note you can also double click the script)
~~~
run_uranos_nogui_windows.bat
~~~

The output should look something like that below.
~~~
stowell-macbook:URANOS_DOCKER stowell$ ./run_uranos_nogui_linux.sh
/uranos/workdir
Example_GUI   Example_SM25  Uranos.cfg		      run_uranos_gui_linux.sh	  run_uranos_nogui_linux.sh
Example_SM10  README.md     UranosGeometryConfig.dat  run_uranos_gui_windows.bat  run_uranos_nogui_windows.bat
QStandardPaths: XDG_RUNTIME_DIR not set, defaulting to '/tmp/runtime-root'
Reading ConfigFilePath
                                                                       ./%%/**
**/,         /((((((((/.        ((     /((((.    ((((   /( (#( (/    ./%%/*    
*%%,   *****  *%%,   ,%%/      /%%/      /%%%/    */  /..#,/#/,#../  *%%*      
*%%,    ./(   *%%,   /%%*     */*%%*     /*,#%#,  */ .*##* *#* *##*. %%%%/*    
*%%,    .*/   *%%(((%%*      *#  #%#.    /* .%%%/.*/  .(#  *#*  #(.   /%%%%%%/
/%%,    .*/   *%%*  ,%%*    *#((((%%(    /*   *%%%%/  .##. *#* .##.        /#%/
/%%,    .*/   /%%*   #%%   *#(    *%%(   /*     (%%/ ,#*,#.*#*.#.*#, #%/    %%/
/%%,.....*/..//%%*..../#%%/#(....../%%/../*......,%/   ,(#,###,#(,   /%%%%%%/  
,%%/    */, ...**.....**%%%(.......((((((((......((((..((.(###(.((..((((%%%/..
 ,#%#%%#/,     ........//////////////////////////////////////////////////***...
                        ...................................................    
GUI disabled
OPENED CONFIGFILE PATH Uranos.cfg
Using Config File Uranos.cfg
Calling Geomtry setup
Cosmic Neutron Spectrum Definition...
Reading Parameters from /uranos/ENDF/

done
Overall layers: 6
Generating Sato 2016 Input Spectrum
Generating Detector Model
Generating Histograms

Reading Angular Cross Sections
Reading from /uranos/ENDF/ angularSi28tabulated.txt
~~~

If you saw that then great, you can now run URANOS without having to worry about all the extra build install requirements like ROOT and C++ files!

Note that this setup is actually quite inefficient when running multiple parrallel jobs, as each job gets its own cloned container. It is possible to have just one running container handling many jobs by mounting custom working directories at runtime and not using the `--rm` flag, but that is beyond the scope of this tutorial.

To run URANOS without the GUI using a custom configuration, you should make a new folder, and copy the config files and nogui script into it.
The scripts automatically get the current directory and mount it into the docker image for you so this way everything is nice and contained.

On Linux for example you can start a new job using the folllowing commands.
~~~
mkdir NEW_URANOS_RUN
cp Uranos.cfg UranosGeometryConfig.dat run_uranos_nogui_linux.sh NEW_URANOS_RUN/
cd NEW_URANOS_RUN
source run_uranos_nogui_linux.sh
~~~

## Running with a GUI
One limitation of docker is that the GUI of URANOS is generated inside the container itself and we need to use a forwarding software such as XWindows to actually display it.

### Windows X Windows setup.
On Windows the recommended package to use is VcXsrv : https://sourceforge.net/projects/vcxsrv/
Once installed you should do the following:
~~~
Run XLaunch from the start menu
Select `Multiple Windows`
Set `Display Number` to 0
Select `Next`
Select `Start No Client`
Click `Disable Access Control`
Click `Next`
Click `Save Configuration` and save file.
Click `Finish`
~~~
Some more detailed notes on how to setup XLaunch for docker can be found here: https://dev.to/darksmile92/run-gui-app-in-linux-docker-container-on-windows-host-4kde

### OSX X Windows Setup
On OSX or Linux, you should be able to just install an X11 program of your choice.
On OSX XQuartz has been found to work well: https://www.xquartz.org


Once you have a working XWindows application, you can run the gui scripts which will automatically setup your X windows display details for docker.

On Linux/OSX
~~~
source run_uranos_gui_linux.sh
~~~

On Windows (note you can also double click the script)
~~~
run_uranos_gui_windows.bat
~~~

If successful you should see the docker GUI show up.

## Docker installation
Docker can be installed using the following link : https://docs.docker.com/get-docker/
This on windows may need to enable the WSL subsystem if it is not already enabled, and possibly hyper-virtualization.

Once docker is installed, start it by clicking `Docker Desktop` in the start menu on windows, or application menu on OSX.
On Linux docker should be setup as a service that starts on boot.

Finally you can check docker is running, by entering the following comment on a terminal/command-prompt.
~~~
$ docker run
latest: Pulling from library/hello-world
2db29710123e: Pull complete
Digest: sha256:53f1bbee2f52c39e41682ee1d388285290c5c8a76cc92b42687eecf38e0af3f0
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
~~~
